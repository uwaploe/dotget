// Dotget downloads a dataset from the DOT Buoy SFTP server
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"time"

	"github.com/pkg/sftp"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `Usage: dotget [options] buoy_id type T_start [T_end]

Download a dataset of the specified type starting at T_start and ending at
T_end (defaults to now) from the DOT Buoy SFTP server. Data is written
to standard output unless the --output option is used. The start and end
times are specified using the RFC3339 format:

YYYY-mm-dd[THH:MM[:SS]]

Buoy_id is dot-1, dot-2, dot-3, etc. Valid file types are "metavg", "met",
"meas", "pvt", "pr", and "sens".
`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	outFile            = flag.String("output", "", "Output file name")
	dotServer   string = "10.95.97.213:22"
	sftpKeyfile string
)

const (
	DOT_USER = "dot"
	DATADIR  = "/home/dot/public_html"
)

// parseDatetime is a flexible ISO8601-ish date/time parser. It handles
// the following formats (missing least-significant values are assumed
// to be zero):
//
//    YYYY-mm-ddTHH:MM:SS-ZZZZ
//    YYYY-mm-ddTHH:MM:SS   (UTC assumed)
//    YYYY-mm-ddTHH:MM
//    YYYY-mm-dd
//
func parseDatetime(dt string) (time.Time, error) {
	t, err := time.Parse("2006-01-02T15:04:05-0700", dt)
	if err != nil {
		t, err = time.Parse("2006-01-02T15:04:05", dt)
		if err != nil {
			t, err = time.Parse("2006-01-02T15:04", dt)
			if err != nil {
				t, err = time.Parse("2006-01-02", dt)
			}
		}
	}

	return t.UTC(), err
}

func lookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}
	flag.StringVar(&sftpKeyfile, "key", lookupEnvOrString("DOT_SSHKEY", sftpKeyfile),
		"SSH private key file")
	flag.StringVar(&dotServer, "host", lookupEnvOrString("DOT_HOST", dotServer),
		"DOT server host")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	if len(args) < 3 {
		flag.Usage()
		os.Exit(1)
	}

	dotId := args[0]

	var (
		t_start, t_end time.Time
		err            error
	)

	t_start, err = parseDatetime(args[2])
	if err != nil {
		log.Fatalf("Bad start time %q: %v", args[2], err)
	}

	if len(args) > 3 {
		t_end, err = parseDatetime(args[3])
		if err != nil {
			log.Fatalf("Bad end time %q: %v", args[3], err)
		}
	} else {
		t_end = time.Now().UTC()
	}

	var fout io.WriteCloser
	if *outFile != "" {
		fout, err = os.OpenFile(*outFile, os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatalf("Cannot create output file: %v", err)
		}
		defer fout.Close()
	} else {
		fout = os.Stdout
	}

	conn, err := pubkeyConnect("dot", dotServer, sftpKeyfile)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	c, err := sftp.NewClient(conn)
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()

	rmHeader := func(dst io.Writer, src io.Reader) (int64, error) {
		var (
			total int64
			n     int
			err   error
		)
		sc := bufio.NewScanner(src)
		line := int(0)
		for sc.Scan() {
			line++
			if line == 1 {
				continue
			}
			n, err = dst.Write(sc.Bytes())
			if err != nil {
				return total, err
			}
			dst.Write([]byte("\n"))
			total += int64(n)
		}
		return total, sc.Err()
	}

	dataType := args[1]
	switch fileExts[dataType] {
	case "csv":
		count := int(0)
		for _, name := range ListFiles(dotId, dataType, t_start, t_end) {
			if f, err := c.Open(c.Join(DATADIR, name)); err == nil {
				log.Printf("Adding %s", name)
				if count == 0 {
					_, err = io.Copy(fout, f)
				} else {
					_, err = rmHeader(fout, f)
				}
				if err != nil {
					f.Close()
					log.Fatal(err)
				}
				count++
				f.Close()
			}
		}
	case "":
		log.Printf("Invalid file type: %q", dataType)
		os.Exit(2)
	default:
		for _, name := range ListFiles(dotId, dataType, t_start, t_end) {
			if f, err := c.Open(c.Join(DATADIR, name)); err == nil {
				log.Printf("Adding %s", name)
				f.WriteTo(fout)
			}
		}
	}

}

module bitbucket.org/uwaploe/dotget

go 1.17

require (
	github.com/pkg/errors v0.8.1
	github.com/pkg/sftp v1.10.1
	golang.org/x/crypto v0.0.0-20190923035154-9ee001bba392
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.1.0 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3 // indirect
	golang.org/x/sys v0.0.0-20190922100055-0a153f010e69 // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/check.v1 v0.0.0-20161208181325-20d25e280405 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)

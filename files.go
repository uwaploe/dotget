package main

import (
	"fmt"
	"time"
)

var fileExts map[string]string = map[string]string{
	"meas":   "sbf",
	"pvt":    "sbf",
	"pr":     "csv",
	"tcm":    "csv",
	"metavg": "csv",
	"met":    "csv",
	"sens":   "csv",
}

// ListFiles generates a list of all possible file paths between the start
// time (exclusive) and end time (exclusive).
func ListFiles(id, measurement string, t_start, t_end time.Time) []string {
	paths := make([]string, 0)
	ext := fileExts[measurement]
	if ext == "" {
		return paths
	}

	for t := t_start.Add(time.Hour); t.Before(t_end); t = t.Add(time.Hour) {
		path := fmt.Sprintf("%s/%s/%d/%03d/%s-%s.%s",
			id, measurement,
			t.Year(),
			t.YearDay(),
			measurement,
			t.Format("20060102-15"), ext)
		paths = append(paths, path)
	}

	return paths
}

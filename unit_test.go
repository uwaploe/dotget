package main

import (
	"testing"
	"time"
)

var table = []struct {
	id, dtype string
	t0, t1    time.Time
	paths     []string
}{
	{
		id:    "dot-1",
		dtype: "pr",
		t0:    time.Date(2000, 1, 1, 22, 1, 0, 0, time.UTC),
		t1:    time.Date(2000, 1, 2, 2, 1, 0, 0, time.UTC),
		paths: []string{
			"dot-1/pr/2000/001/pr-20000101-23.csv",
			"dot-1/pr/2000/002/pr-20000102-00.csv",
			"dot-1/pr/2000/002/pr-20000102-01.csv",
		},
	},
}

func TestFileList(t *testing.T) {
	for _, e := range table {
		paths := ListFiles(e.id, e.dtype, e.t0, e.t1)
		for i, p := range paths {
			if p != e.paths[i] {
				t.Errorf("Path mismatch; expected %q, got %q",
					e.paths[i], p)
			}
		}
	}
}

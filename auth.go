package main

import (
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"

	"github.com/pkg/errors"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/terminal"
)

func getPassword(prompt string) ([]byte, error) {
	fd := int(os.Stdin.Fd())
	state, err := terminal.GetState(fd)
	if err != nil {
		return nil, err
	}

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Interrupt, os.Kill)
	defer signal.Stop(ch)

	go func() {
		_, ok := <-ch
		if ok {
			terminal.Restore(fd, state)
			os.Exit(1)
		}
	}()

	fmt.Print(prompt)
	pass, err := terminal.ReadPassword(fd)
	fmt.Println("")

	return pass, err
}

func readPrivateKey(filename string) (ssh.Signer, error) {
	contents, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, errors.Wrap(err, "read key file")
	}

	block, _ := pem.Decode(contents)

	if x509.IsEncryptedPEMBlock(block) {
		fmt.Printf("%s is password protected\n", filename)
		pass, err := getPassword("Enter Password: ")
		if err != nil {
			return nil, errors.Wrap(err, "read password")
		}
		return ssh.ParsePrivateKeyWithPassphrase(contents, pass)
	}

	return ssh.ParsePrivateKey(contents)
}

func pubkeyConnect(user, host, keyFile string) (*ssh.Client, error) {
	signer, err := readPrivateKey(keyFile)
	if err != nil {
		return nil, errors.Wrap(err, "parse key file")
	}

	config := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			// Use the PublicKeys method for remote authentication.
			ssh.PublicKeys(signer),
		},
		// Host authentication is not needed for this application
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	return ssh.Dial("tcp", host, config)
}
